# bio

## Experience

### Use of various technologies over the years 2016-2021


|            | 2016 | 2017 | 2018 | 2019 | 2020 | 2021 | summary** | advancement |
| ---------- | ---- | ---- | ---- | ---- | ---- | ---- | --------- | ----------- |
| coding*    | 100% | 75%  | 85%  | 70%  | 40%  | 17%  | 83%       |             |
| PM         | 0%   | 5%   | 10%  | 25%  | 8%   | 0%   | 10%       |             |
| mentoring* | 0%   | 20%  | 5%   | 5%   | 2%   | 0%   | 7%        |             |
| code days* | 240  | 180  | 204  | 168  | 96   | 40   | 928       |             |
|            |      |      |      |      |      |      |           |             |
| JavaScript | 100% | 100% | 90%  | 60%  | 30%  | 0%   | 733       | 90%         |
| TypeScript | 20%  | 100% | 90%  | 60%  | 30%  | 0%   | 541       | 90%         |
| Node.js    | 100% | 50%  | 20%  | 40%  | 15%  | 0%   | 452       | 85%         |
| Angular    | 0%   | 30%  | 90%  | 30%  | 15%  | 0%   | 302       | 90%         |
| React      | 0%   | 20%  | 0%   | 5%   | 0%   | 0%   | 44        | 50%         |
|            |      |      |      |      |      |      |           |             |
| Flutter    | 0%   | 0%   | 0%   | 0%   | 0%   | 70%  | 28        | 60%         |
| Python     | 0%   | 0%   | 0%   | 10%  | 20%  | 15%  | 42        | 50%         |
| FastAPI    | 0%   | 0%   | 0%   | 0%   | 0%   | 15%  | 6         | 70%         |
|            |      |      |      |      |      |      |           |             |
| PostgreSQL | 80%  | 50%  | 0%   | 20%  | 10%  | 15%  | 331       | 60%         |
| Redis      | 2%   | 2%   | 0%   | 3%   | 2%   | 0%   | 15        | 70%         |
|            |      |      |      |      |      |      |           |             |
| CI/CD*     | 5%   | 5%   | 0%   | 10%  | 10%  | 10%  | 51        | 80%         |
|            |      |      |      |      |      |      |           |             |
| Docker     | 0%   | 0%   | 0%   | 30%  | 30%  | 20%  | 87        | 60%         |
| Kubernetes | 0%   | 0%   | 0%   | 5%   | 50%  | 5%   | 58        | 60%         |
| AWS        | 0%   | 0%   | 10%  | 30%  | 5%   | 0%   | 76        | 30%         |
| GCP        | 0%   | 0%   | 0%   | 0%   | 30%  | 0%   | 29        | 30%         |


\* `coding` row shows the percent of the working time spent on coding - this includes not only individual coding but also pair coding, code reviews, technical specifications, architecture planning, and collaboration.

\* `mentoring` includes activities such as tutoring junior developers, preparing educational materials etc.

\* The number of days per year is calculated as 12 months with ~20 days of work in each, which gives around 240 working days per year - `code days` are a percent of this number.

\* `CI/CD` contains working with Gitlab & Azure Pipelines and their integrations with e.g. Kubernetes clusters

\*\* The summary column for technologies shows how many days in total were spent working with a given technology.



##### Please note that these numbers are based on a retrospective - they depend heavily on my recollection of a given period and projects that I was a part of. The first attempt to compile this table was made on 11th April 2021.
